﻿using AppService.Entity;

namespace AppService.IRepository
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
