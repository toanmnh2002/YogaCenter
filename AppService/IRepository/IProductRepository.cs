﻿using AppService.Entity;

namespace AppService.IRepository
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<List<Product>> GetProductByCategory(int id);
        Task<Product> GetDetail(int id);
    }
}
