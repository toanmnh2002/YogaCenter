﻿using AppService.IRepository;

namespace AppService
{
    public interface IUnitOfWork
    {
        public ICategoryRepository CategoryRepository { get;}
        public IProductRepository ProductRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
