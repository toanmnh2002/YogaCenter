﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class SalaryChangeRequest : BaseEntity
    {
        public int InstructorId { get; set; }
        public decimal NewSalary { get; set; }
        public DateTime RequestDate { get; set; }
        public bool IsApproved { get; set; }
        public Instructor Instructor { get; set; }
    }
}
