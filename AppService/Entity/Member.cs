﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YogaCenterWebApp.Relationship;

namespace AppService.Entity
{
    public class Member:BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public MemberStatus memberStatus { get; set; }
        public MemberRole MemberRole { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public ICollection<Attendance>? Attendances { get; set; }
        public ICollection<EquipmentRental>? EquipmentRentals { get; set; }
        public ICollection<Enrollment>? Enrollments { get; set; }
        public ICollection<Payment>? Payments { get; set; }
        public ICollection<Instructor>? Instructors { get; set; }
    }
    public enum MemberStatus
    {
        Disable, Enable
    }
    public enum MemberRole
    {
        Admin,
        ClassAdmin,
        Student,
        Mentor
    }
}
