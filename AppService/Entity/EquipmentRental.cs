﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class EquipmentRental : BaseEntity
    {
        public int MemberId { get; set; }
        public int EquipmentId { get; set; }
        public DateTime DateRental { get; set; }
        public DateTime DateReturn { get; set; }
        public bool IsApproved { get; set; }
        public Member Member { get; set; }
        public Equipment Equipment { get; set; }
    }
}
