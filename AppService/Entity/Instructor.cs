﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class Instructor:BaseEntity
    {
        public int MemberId { get; set; }
        public decimal Salary { get; set; }
        public Member Member { get; set; }
        public ICollection<Class>? Classes { get; set; }
        public ICollection<SalaryChangeRequest>? SalaryChangeRequests { get; set; }
        public ICollection<EventRequest>? EventRequests { get; set; }
    }
}
