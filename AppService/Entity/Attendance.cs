﻿using AppService.Entity;

namespace YogaCenterWebApp.Relationship
{
    public class Attendance : BaseEntity
    {
        public int ClassId { get; set; }
        public int MemberId { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string Note { get; set; }
        public bool IsPresent { get; set; }
        public Member Member { get; set; }
        public Class Class { get; set; }
    }
    public enum AttendanceEnumStatus
    {
        Absent,
        Present,
        AllowAbsent
    }
}