﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class Payment : BaseEntity
    {
        public int MemberId { get; set; }
        public int ClassId { get; set; }
        public decimal Money { get; set; }
        public DateTime PaymentDate { get; set; }
        public Class Class { get; set; }
        public Member Member { get; set; }
    }
}
