﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class Equipment : BaseEntity
    {
        public string EquipmentName { get; set; }
        public int Quantity { get; set; }
        public ICollection<EquipmentRental>? EquipmentRentals { get; set; }

    }
}
