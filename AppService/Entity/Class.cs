﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YogaCenterWebApp.Relationship;

namespace AppService.Entity
{
    public class Class : BaseEntity
    {
        public string ClassName { get; set; }
        public int InstructorId { get; set; }
        public int RoomId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public ICollection<Attendance>? Attendances { get; set; }
        public ICollection<Enrollment>? Enrollments { get; set; }
        public ICollection<Payment>? Payments { get; set; }
        public Instructor Instructor { get; set; }
        public ICollection<EventRequest>? EventRequests { get; set; }
        public Room Room { get; set; }


    }
}
