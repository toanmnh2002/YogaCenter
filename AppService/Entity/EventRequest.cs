﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class EventRequest : BaseEntity
    {
        public int InstuctorId { get; set; }
        public int ClassId { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public bool IsApproved { get; set; }
        public Instructor Instructor { get; set; }
        public Class Class { get; set; }

    }
}
