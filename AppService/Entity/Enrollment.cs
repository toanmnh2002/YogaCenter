﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class Enrollment:BaseEntity
    {
        public int MemberId { get; set; }
        public int ClassId { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public Class Class { get; set; }
        public Member Member { get; set; }
    }
}
