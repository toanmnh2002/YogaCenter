﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Entity
{
    public class Room : BaseEntity
    {
        public string RoomName { get; set; }
        public int Capacity { get; set; }
        public ICollection<Class> Classes { get; set; }
    }
}
