﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using YogaCenterWebApp.Relationship;

namespace YogaCenterWebApp
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        //public DbSet<Product> Products { get; set; }
        //public DbSet<Category> Categories { get; set; }
        public DbSet<Attendance> Attendance { get; set; }
        public DbSet<Class> Class { get; set; }
        public DbSet<Enrollment> Enrollment { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<EquipmentRental> EquipmentRental { get; set; }
        public DbSet<EventRequest> EventRequest { get; set; }
        public DbSet<Instructor> Instructor { get; set; }
        public DbSet<Member> Member { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<Room> Room { get; set; }
        public DbSet<SalaryChangeRequest> SalaryChangeRequest { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
