﻿using AppService;
using AppService.IRepository;
using AppService.IService;
using Microsoft.EntityFrameworkCore;
using YogaCenterWebApp.Repository;
using YogaCenterWebApp.Service;

namespace YogaCenterWebApp
{
    public static class DependencyInjection
    {
        public static IServiceCollection AppService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllersWithViews();
            services.AddDbContext<AppDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("YogaDB")));

            //Register service
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IProductService, ProductService>();

            services.AddSession();
            return services;
        }
    }
}
