﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace YogaCenterWebApp.Controllers
{
    public class ProductController : BaseController
    {
        public ProductController(IProductService productService, ICategoryService categoryService) : base(productService, categoryService)
        {
        }

        public async Task<IActionResult> Index()
        {
            var result = await _productService.GetAllProduct();
            return View(result);
        }
        public async Task<IActionResult> ProductDetail([FromRoute] int id)
        {
            var result = await _productService.GetProductById(id);
            return View(result);
        }
        public async Task<IActionResult> ProductCategory([FromRoute] int id)
        {
            var result = await _productService.GetByCategory(id);
            return View(result);
        }
    }
}
