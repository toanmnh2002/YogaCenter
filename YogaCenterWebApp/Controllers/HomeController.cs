﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace YogaCenterWebApp.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IProductService productService, ICategoryService categoryService) : base(productService, categoryService)
        {
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Cart()
        {
            return View();
        }
        public IActionResult ProductDetail()
        {
            return View();
        }
        public IActionResult Checkout()
        {
            return View();
        }
        public IActionResult Shop()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
    }
}
