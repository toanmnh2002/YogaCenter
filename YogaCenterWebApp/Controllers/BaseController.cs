﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using YogaCenterWebApp.Models;

namespace YogaCenterWebApp.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IProductService _productService;
        protected readonly ICategoryService _categoryService;
        public BaseController(IProductService productService, ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
