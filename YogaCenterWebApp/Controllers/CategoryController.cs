﻿using AppService.IService;
using Microsoft.AspNetCore.Mvc;

namespace YogaCenterWebApp.Controllers
{
    public class CategoryController : BaseController
    {
        public CategoryController(IProductService productService, ICategoryService categoryService) : base(productService, categoryService)
        {
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
