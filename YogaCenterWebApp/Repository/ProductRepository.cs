﻿using AppService.Entity;
using AppService.IRepository;
using Microsoft.EntityFrameworkCore;

namespace YogaCenterWebApp.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Product> GetDetail(int id) => await _dbSet.Include(x => x.Category).FirstOrDefaultAsync(x => x.Id == id);

        public async Task<List<Product>> GetProductByCategory(int id)
        {
            List<Product> products = await _dbSet.Where(x => x.CategoryId==id).ToListAsync();
            return products;
        }
    }
}
