﻿using AppService.Entity;
using AppService.IRepository;
using Microsoft.EntityFrameworkCore;

namespace YogaCenterWebApp.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected DbSet<T> _dbSet;

        public GenericRepository(AppDbContext dbContext)
        {
            _dbSet = dbContext.Set<T>();
        }

        public async Task CreateAsync(T entity) => await _dbSet.AddAsync(entity);

        public void DeleteAsync(T entity) => _dbSet.Remove(entity);

        public async Task<List<T>?> GetAllAsync() => await _dbSet.ToListAsync();

        public async Task<T?> GetAsync(int? id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public void UpdateAsync(T entity) => _dbSet.Update(entity);
    }
}
