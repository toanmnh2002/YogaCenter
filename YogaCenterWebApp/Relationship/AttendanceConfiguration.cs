﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class AttendanceConfiguration : IEntityTypeConfiguration<Attendance>
    {
        public void Configure(EntityTypeBuilder<Attendance> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.ClassId).HasColumnName("ClassId");
            builder.Property(e => e.MemberId).HasColumnName("MemberId");
            builder.Property(e => e.AttendanceDate)
                .HasColumnType("datetime");
            builder.HasOne(d => d.Class).WithMany(p => p.Attendances)
                .HasForeignKey(d => d.ClassId)
                .HasConstraintName("FK__Attendanc__Class");
            builder.HasOne(d => d.Member).WithMany(p => p.Attendances)
                .HasForeignKey(d => d.MemberId)
                .HasConstraintName("FK__Attendanc__Member").OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}
