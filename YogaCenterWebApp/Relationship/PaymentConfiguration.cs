﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.ClassId).HasColumnName("ClassId");
            builder.Property(e => e.MemberId).HasColumnName("MemberId");
            builder.Property(e => e.Money)
                .HasColumnType("decimal(10, 2)");
            builder.Property(e => e.PaymentDate)
               .HasColumnType("datetime");
            builder.HasOne(x => x.Class)
                .WithMany(x => x.Payments)
                .HasForeignKey(x => x.ClassId)
                            .HasConstraintName("FK__Payments__Class");
            builder.HasOne(x => x.Member)
                .WithMany(x => x.Payments)
                .HasForeignKey(x => x.MemberId)
                .OnDelete(DeleteBehavior.ClientCascade)
                            .HasConstraintName("FK__Payments__Member");

        }
    }
}
