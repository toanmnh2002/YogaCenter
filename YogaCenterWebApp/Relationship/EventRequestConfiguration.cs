﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class EventRequestConfiguration : IEntityTypeConfiguration<EventRequest>
    {
        public void Configure(EntityTypeBuilder<EventRequest> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.ClassId).HasColumnName("ClassId");
            builder.Property(e => e.InstuctorId).HasColumnName("InstuctorId");
            builder.Property(e => e.EventDate)
              .HasColumnType("datetime");
            builder.HasOne(x => x.Class)
                .WithMany(x => x.EventRequests)
                .HasForeignKey(x => x.ClassId)
                .HasConstraintName("FK__EventRequest__Class__47DBAE45");
            builder.HasOne(x => x.Instructor)
                .WithMany(x => x.EventRequests)
                .HasForeignKey(x => x.InstuctorId)
                .OnDelete(DeleteBehavior.ClientCascade)
                .HasConstraintName("FK__EventRequest__Instructor__46E78A0C");
        }
    }
}
