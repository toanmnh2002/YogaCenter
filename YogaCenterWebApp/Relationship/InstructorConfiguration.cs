﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class InstructorConfiguration : IEntityTypeConfiguration<Instructor>
    {
        public void Configure(EntityTypeBuilder<Instructor> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.MemberId).HasColumnName("MemberId");
            builder.Property(e => e.Salary)
                .HasColumnType("decimal(10, 2)");
            builder.HasOne(x => x.Member)
                .WithMany(x => x.Instructors)
                .HasForeignKey(x => x.MemberId)
                                .HasConstraintName("FK__Instructor__Member");
        }
    }
}
