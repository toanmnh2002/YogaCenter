﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class EnrollmentConfiguration : IEntityTypeConfiguration<Enrollment>
    {
        public void Configure(EntityTypeBuilder<Enrollment> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.MemberId).HasColumnName("MemberId");
            builder.Property(e => e.ClassId).HasColumnName("ClassId");
            builder.Property(e => e.EnrollmentDate).HasColumnType("datetime");
            builder.HasOne(x => x.Member)
                .WithMany(x => x.Enrollments)
                .HasForeignKey(x => x.MemberId)
                .HasConstraintName("FK__Enrollment__Member");
            builder.HasOne(x => x.Class)
                .WithMany(x => x.Enrollments)
                .HasForeignKey(x => x.ClassId).OnDelete(DeleteBehavior.ClientCascade)
                .HasConstraintName("FK__Enrollment__Class");
        }
    }
}
