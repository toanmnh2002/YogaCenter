﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class ClassConfiguration : IEntityTypeConfiguration<Class>
    {
        public void Configure(EntityTypeBuilder<Class> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.RoomId).HasColumnName("RoomId");
            builder.Property(e => e.InstructorId).HasColumnName("InstructorId");
            builder.Property(e => e.StartTime)
                .HasColumnType("datetime");
            builder.Property(e => e.EndTime)
               .HasColumnType("datetime");
            builder.HasOne(x => x.Room)
                .WithMany(x => x.Classes)
                .HasForeignKey(u => u.RoomId)
                .HasConstraintName("FK__Classes__Room");
            builder.HasOne(x => x.Instructor)
                .WithMany(x => x.Classes)
                .HasForeignKey(x => x.InstructorId)
                .HasConstraintName("FK__Classes__Instructor");
        }
    }
}
