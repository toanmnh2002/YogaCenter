﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class EquipmentRentalConfiguration : IEntityTypeConfiguration<EquipmentRental>
    {
        public void Configure(EntityTypeBuilder<EquipmentRental> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.EquipmentId).HasColumnName("EquipmentId");
            builder.Property(e => e.MemberId).HasColumnName("MemberId");
            builder.Property(e => e.DateRental).HasColumnType("datetime");
            builder.Property(e => e.DateReturn).HasColumnType("datetime");
            builder.HasOne(x => x.Equipment)
                   .WithMany(x => x.EquipmentRentals)
                   .HasForeignKey(x => x.EquipmentId)
                   .HasConstraintName("FK__EquipmentRental__Equipment");
            builder.HasOne(x => x.Member)
                .WithMany(x => x.EquipmentRentals)
                .HasForeignKey(x => x.MemberId)
                .HasConstraintName("FK__EquipmentRental__Member__4222D4EF");
            ;
        }
    }
}
