﻿using AppService.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace YogaCenterWebApp.Relationship
{
    public class SalaryChangeRequestConfiguration : IEntityTypeConfiguration<SalaryChangeRequest>
    {
        public void Configure(EntityTypeBuilder<SalaryChangeRequest> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.InstructorId).HasColumnName("InstructorId");
            builder.Property(e => e.NewSalary)
           .HasColumnType("decimal(10, 2)");
            builder.Property(e => e.RequestDate)
               .HasColumnType("datetime");
            builder.HasOne(d => d.Instructor).WithMany(p => p.SalaryChangeRequests)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK__SalaryChangeRequest__Instructor");
        }
    }
}
