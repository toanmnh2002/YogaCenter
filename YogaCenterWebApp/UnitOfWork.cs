﻿using AppService;
using AppService.IRepository;
using YogaCenterWebApp.Repository;

namespace YogaCenterWebApp
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public ICategoryRepository CategoryRepository => new CategoryRepository(_appDbContext);

        public IProductRepository ProductRepository => new ProductRepository(_appDbContext);

        public async Task<int> SaveChangeAsync() => await _appDbContext.SaveChangesAsync();
    }
}
