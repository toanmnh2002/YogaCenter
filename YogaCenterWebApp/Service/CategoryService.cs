﻿using AppService;
using AppService.Entity;
using AppService.IService;

namespace YogaCenterWebApp.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Category> CreateCategory(Category category)
        {
            await _unitOfWork.CategoryRepository.CreateAsync(category);
            await _unitOfWork.SaveChangeAsync();
            return category;
        }

        public async Task<bool> DeleteCategory(int id)
        {
            var result = await _unitOfWork.CategoryRepository.GetAsync(id);
            if(result == null)
            {
                throw new Exception("Not Found");
            }
            _unitOfWork.CategoryRepository.DeleteAsync(result);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public async Task<List<Category>> GetCategory() => await _unitOfWork.CategoryRepository.GetAllAsync();

        public async Task<Category> GetCategory(int id)
        {
            var result = await _unitOfWork.CategoryRepository.GetAsync(id);
            if (result is null)
            {
                throw new Exception($"{nameof(Category)} is null");
            }
            return result;
        }

        public async Task<Category> UpdateCategory(Category category)
        {
            _unitOfWork.CategoryRepository.UpdateAsync(category);
            await _unitOfWork.SaveChangeAsync();
            return category;
        }
    }
}
