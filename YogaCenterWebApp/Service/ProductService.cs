﻿using AppService;
using AppService.Entity;
using AppService.IService;

namespace YogaCenterWebApp.Service
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> AddProduct(Product product)
        {
            await _unitOfWork.ProductRepository.CreateAsync(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if(isSuccess < 0)
            {
                throw new Exception("Add fail");
            }
            return true;
        }

        public async Task<bool> DeleteProduct(int id)
        {
            var result = await _unitOfWork.ProductRepository.GetAsync(id);
            if(result is null)
            {
                throw new Exception($"Product {id} does not exist");
            }
            _unitOfWork.ProductRepository.DeleteAsync(result);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if(isSuccess < 0)
            {
                throw new Exception("Delete fail");
            }
            return true;
        }

        public async Task<List<Product>?> GetAllProduct()
        {
            var result = await _unitOfWork.ProductRepository.GetAllAsync();
            if(result is null)
            {
                return null;
            }
            return result;
        }

        public async Task<List<Product>> GetByCategory(int id) => await _unitOfWork.ProductRepository.GetProductByCategory(id);

        public async Task<Product> GetProductById(int id)
        {
            var result = await _unitOfWork.ProductRepository.GetDetail(id);
            if( result is null)
            {
                throw new Exception($"Product {id} does not exist");
            }
            return result;
        }

        public async Task<bool> UpdateProduct(Product product)
        {
            _unitOfWork.ProductRepository.DeleteAsync(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if(isSuccess < 0)
            {
                throw new Exception("Update fail");
            }
            return true;
        }
    }
}
